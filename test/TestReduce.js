const elements = [1, 2, 3, 4, 5, 5];
const reduce = require("../reduce");
try {
  const sum = reduce.reduce(elements, (acc, curr) => acc + curr, 0);

  console.log(sum);
} catch (error) {
  console.log(error);
}
