function each(element, cb) {
  for (let index = 0; index < element.length; index++) {
    cb(element[index]);
  }
}
module.exports = { each };
