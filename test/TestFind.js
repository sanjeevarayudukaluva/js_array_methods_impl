const elements = [1, 2, 3, 4, 5, 5];
const find = require("../find.js");

try {
  function callBack(element, arr) {
    for (let index = 0; index < arr.length; index++) {
      if (element === 5) {
        return element;
      }
    }
  }
  console.log(find.find(elements, callBack));
} catch (error) {
  console.log(error);
}
