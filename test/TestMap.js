const map = require("../map");
const element = [1, 2, 3, 4, 5, 5];
try {
  function callBack(element) {
    return element * 2;
  }
  console.log(map.map(element, callBack));
} catch (error) {
  console.log(error);
}
