const filter = require("../filter");
const elements = [1, 2, 3];
try {
  function callBack(element) {
    return element > 3;
  }
  console.log(filter.filter(elements, callBack));
} catch (error) {
  console.log(error);
}
