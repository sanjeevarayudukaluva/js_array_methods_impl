function reduce(element, cb, startingValue) {
  let accumulator = startingValue !== undefined ? 0 : element[0];
  const startIndex = startingValue !== undefined ? 0 : 1;
  for (let index = startIndex; index < element.length; index++) {
    accumulator = cb(accumulator, element[index]);
  }
  return accumulator;
}
module.exports = { reduce };
