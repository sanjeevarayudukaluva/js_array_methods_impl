function map(element, cb) {
  const arr = [];
  for (let index = 0; index < element.length; index++) {
    const array = cb(element[index]);
    arr.push(array);
  }
  return arr;
}
module.exports = { map };
