const forEach = require("../forEach.js");
const element = [1, 2, 3, 4, 5, 5];
try {
  function callBack(element) {
    console.log(element);
  }

  const result = forEach.each(element, callBack);
} catch (error) {
  console.log(error);
}
