Array.prototype.myFlat = function (depth = 1) {
  const flattenArray = [];
  function flattener(arr, depth) {
    for (let element of arr) {
      if (Array.isArray(element) && depth) {
        flattener(element, depth - 1);
      } else {
        flattenArray.push(element);
      }
    }
  }
  flattener(this, depth);
  return flattenArray;
};
module.exports = Array.prototype.myFlat;
