try {
  const nestedArray = [1, [2], [[3]], [[[4]]]];

  const myFlat = require("../Flatten");
  console.log("this is my method ", nestedArray.myFlat(Infinity));
} catch (error) {
  console.log(error);
}
